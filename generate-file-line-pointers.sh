#!/usr/bin/env bash

mvn exec:java -Dstart-class="szczepanski.gerard.filelinepointersgenerator.TextFileLinePointersGenerator" -Dexec.args="$1 $2"