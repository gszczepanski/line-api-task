#!/usr/bin/env bash

mvn exec:java -Dstart-class="szczepanski.gerard.textfilegenerator.RandomTextFileGenerator" -Dexec.args="$1 $2"