#!/bin/bash

filePath=$(readlink -f $1)
filename=$(basename $filePath)

./generate-file-line-pointers.sh $filePath 1000

pointersMapPath=flpm-$filename.json

mvn spring-boot:run -Dstart-class="szczepanski.gerard.textprovider.TextProviderApiConfig" -Dspring-boot.run.arguments="--text-file.path=$filePath,--text-file.pointers-map-path=$pointersMapPath"