package szczepanski.gerard.filelinepointersgenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinesPointersBlocksMap;
import java.io.File;

import lombok.RequiredArgsConstructor;


import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static java.util.Objects.nonNull;

@RequiredArgsConstructor
class FileLinePointersBlocksMapWriter {

    private static final String MAP_NAME_TEMPLATE = "flpm-%s.json";

    private final String textFileName;

    private final ObjectMapper mapper;

    void saveToDisk(FileLinesPointersBlocksMap map) {
        checkArgument(nonNull(map), "map is null");

        String filename = format(MAP_NAME_TEMPLATE, textFileName);
        saveFileLinePointersBlocksMapOnDisk(map, filename);
    }

    private void saveFileLinePointersBlocksMapOnDisk(FileLinesPointersBlocksMap map, String filename) {
        File file = new File(filename);
        try {
            mapper.writeValue(file, map);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
