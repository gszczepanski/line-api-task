package szczepanski.gerard.filelinepointersgenerator;

import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinePointersBlock;
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinesPointersBlocksMap;
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinesPointersBlocksMap.FileLinePointersBlockInfo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lombok.RequiredArgsConstructor;


import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.nonNull;

@RequiredArgsConstructor
class FileLinePointersGenerator {

    private final int pointersBlockSize;

    private final FileLinePointersWriter fileLinePointersWriter;

    private final FileLinePointersBlocksMapWriter fileLinePointersBlocksMapWriter;

    public void createFileLinePointersFor(File textFile) {
        checkArgument(nonNull(textFile), "textFile is null");

        try (LinePositionReader reader = new LinePositionReader(new BufferedReader(new FileReader(textFile)))) {
            createTextLinePointersFrom(reader);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void createTextLinePointersFrom(LinePositionReader reader) throws IOException {
        List<FileLinePointersBlockInfo> fileLinePointersBlockInfoList = new ArrayList<>();
        int totalLines = 0;
        int blockNumber = 0;

        while (true) {
            FileLinePointersBlock pointersBlock = createFileLinePointersFor(blockNumber, reader);
            if (pointersBlock == null) break;

            savePointersBlockToDisk(fileLinePointersBlockInfoList, blockNumber, pointersBlock);
            totalLines += pointersBlock.size();

            if (isPointersBlockSizeLowerThanRequired(pointersBlock)) {
                break;
            }
            blockNumber++;
        }
        createPointersBlockMapOnDisk(fileLinePointersBlockInfoList, totalLines);
    }

    private void createPointersBlockMapOnDisk(List<FileLinePointersBlockInfo> fileLinePointersBlockInfoList,
                                              int totalLines) {
        FileLinesPointersBlocksMap map = new FileLinesPointersBlocksMap(pointersBlockSize, totalLines, fileLinePointersBlockInfoList);
        fileLinePointersBlocksMapWriter.saveToDisk(map);
    }

    private boolean isPointersBlockSizeLowerThanRequired(FileLinePointersBlock pointersBlock) {
        return pointersBlock.size() != pointersBlockSize;
    }

    private void savePointersBlockToDisk(List<FileLinePointersBlockInfo> fileLinePointersBlockInfoList, int blockNumber,
                                         FileLinePointersBlock pointers) {
        String fileLinePointersName = fileLinePointersWriter.saveToDisk(pointers, blockNumber);
        fileLinePointersBlockInfoList.add(
                new FileLinePointersBlockInfo(
                        pointers.getFirstLineNumber(), pointers.getLastLineNumber(), fileLinePointersName
                )
        );
    }

    private FileLinePointersBlock createFileLinePointersFor(int blockNumber, LinePositionReader reader) throws IOException {
        long[] linePointers = new long[pointersBlockSize + 1];
        int counter = 0;
        linePointers[counter] = reader.getPosition();
        boolean isAtLeastOneLineProcessed = false;

        while (counter < pointersBlockSize && reader.readLine() != null) {
            isAtLeastOneLineProcessed = true;
            linePointers[++counter] = reader.getPosition();
        }

        return isAtLeastOneLineProcessed ? createFileLinePointersBlockFrom(blockNumber, linePointers, counter) : null;
    }

    private FileLinePointersBlock createFileLinePointersBlockFrom(int blockNumber, long[] linePointers, int counter) {
        int startLineNumber = blockNumber * pointersBlockSize;
        int endLineNumber = startLineNumber + counter - 1;
        return FileLinePointersBlock.from(startLineNumber, endLineNumber, linePointers);
    }

}
