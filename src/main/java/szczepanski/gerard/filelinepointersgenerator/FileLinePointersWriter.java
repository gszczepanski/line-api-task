package szczepanski.gerard.filelinepointersgenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinePointersBlock;
import java.io.File;

import lombok.RequiredArgsConstructor;


import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PACKAGE;

@RequiredArgsConstructor(access = PACKAGE)
class FileLinePointersWriter {

    private static final String FILE_LINE_POINTER_PREFIX = "flp-";

    private static final String POINTERS_BLOCK_FILE_NAME_TEMPLATE = "%s/flp-%d.json";

    private final String pointersDirectoryName;

    private final ObjectMapper mapper;

    static FileLinePointersWriter createForFileName(String fileName, ObjectMapper mapper) {
        String pointersDirectoryName = FILE_LINE_POINTER_PREFIX + fileName;
        return new FileLinePointersWriter(pointersDirectoryName, mapper);
    }

    String saveToDisk(FileLinePointersBlock pointers, int blockNumber) {
        checkArgument(nonNull(pointers), "pointers are null");
        checkArgument(blockNumber > -1, "blockNumber is negative");

        createPointersDirectoryIfNotExist();

        String pointersFileName = format(POINTERS_BLOCK_FILE_NAME_TEMPLATE, pointersDirectoryName, blockNumber);
        saveFileLinePointersBlockOnDisk(pointers, pointersFileName);

        return pointersFileName;
    }

    private void createPointersDirectoryIfNotExist() {
        File directory = new File(pointersDirectoryName);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    private void saveFileLinePointersBlockOnDisk(FileLinePointersBlock pointers, String filename) {
        File file = new File(filename);
        try {
            mapper.writeValue(file, pointers);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
