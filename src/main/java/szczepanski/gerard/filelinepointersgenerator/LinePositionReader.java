package szczepanski.gerard.filelinepointersgenerator;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class LinePositionReader implements Closeable {

    private static final int LF_CHAR_SIZE = 1;

    private final BufferedReader bufferedReader;

    /**
     * Current byte position for file.
     */
    @Getter
    private long position;

    public String readLine() throws IOException {
        String line = bufferedReader.readLine();
        if (line != null) {
            position += line.length() + LF_CHAR_SIZE;
        }
        return line;
    }

    public void close() throws IOException {
        bufferedReader.close();
    }

}
