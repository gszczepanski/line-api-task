package szczepanski.gerard.filelinepointersgenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;


import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.PropertyAccessor.FIELD;
import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

class TextFileLinePointersGenerator {

    public static void main(String... args) {
        checkArgument(args.length == 2, "You should provide 2 arguments: the text file path and block size number");
        checkArgument(isNotBlank(args[0]), "Provided path is blank");
        checkArgument(isNumeric(args[1]), "Provided number of text lines is not numeric");
        checkArgument(Integer.valueOf(args[1]) > 0, "Provided blockSize  must be greater than zero");

        String textFilePath = args[0];
        File textFile = new File(textFilePath);
        int pointersBlockSize = Integer.valueOf(args[1]);

        FileLinePointersGenerator pointersFactory = createFileLinePointersGenerator(textFile.getName(), pointersBlockSize);
        pointersFactory.createFileLinePointersFor(textFile);
    }

    private static FileLinePointersGenerator createFileLinePointersGenerator(String textFileName, int pointersBlockSize) {
        ObjectMapper objectMapper = createObjectMapper();

        FileLinePointersWriter fileLinePointersWriter = FileLinePointersWriter.createForFileName(textFileName, objectMapper);
        FileLinePointersBlocksMapWriter fileLinePointersBlocksMapWriter = new FileLinePointersBlocksMapWriter(textFileName, objectMapper);

        return new FileLinePointersGenerator(
                pointersBlockSize, fileLinePointersWriter, fileLinePointersBlocksMapWriter
        );
    }

    private static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(FIELD, ANY);
        return mapper;
    }
}
