package szczepanski.gerard.textfilegenerator;

import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.text.TextProducer;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;


import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNumeric;

class RandomTextFileGenerator {

    private static final TextProducer TEXT_PRODUCER = Fairy.create().textProducer();

    private static final String CHARSET_NAME = "UTF-8";

    public static void main(String... args) {
        validateIncomingParams(args);
        String filename = args[0];
        int linesNumber = Integer.valueOf(args[1]);

        try (Writer writer = new PrintWriter(filename, CHARSET_NAME)) {
            writeLinesToFile(writer, linesNumber);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void validateIncomingParams(String... args) {
        checkArgument(args.length == 2, "You should provide 2 arguments: filename and number of text lines");
        checkArgument(isNotEmpty(args[0]), "Provided filename is empty");
        checkArgument(isNumeric(args[1]), "Provided number of text lines is not numeric");
        checkArgument(Integer.valueOf(args[1]) > 0, "Provided number of text lines must be greater than zero!");
    }

    private static void writeLinesToFile(Writer writer, int numberOfLines) throws IOException {
        for (int i = 0; i < numberOfLines; i++) {
            writer.write(format("[%d] %s\n", i, TEXT_PRODUCER.sentence()));
        }
    }
}
