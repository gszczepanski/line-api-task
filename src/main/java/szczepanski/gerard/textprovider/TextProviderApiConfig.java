package szczepanski.gerard.textprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "szczepanski.gerard.textprovider")
public class TextProviderApiConfig {

    public static void main(String... args) {
        SpringApplication.run(TextProviderApiConfig.class, args);
    }

}
