package szczepanski.gerard.textprovider.boundary.line;

import szczepanski.gerard.textprovider.domain.line.TextLineRepository;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;


import static org.apache.commons.lang3.StringUtils.isNumeric;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/lines")
@RequiredArgsConstructor
class TextLineController {

    private final TextLineRepository textLineRepository;

    @GetMapping("/{line-number}")
    ResponseEntity<String> getTextLine(@PathVariable(value = "line-number") String textLineNumber) {
        if (!isNumeric(textLineNumber)) return responseNotFound();

        int lineNumber = assembleLineNumber(textLineNumber);
        if (!isValidForProcessing(lineNumber)) return responseNotFound();

        return textLineRepository.findBy(lineNumber)
                .map(textLine -> ok(textLine))
                .orElse(responseNotFound());
    }

    private ResponseEntity<String> responseNotFound() {
        return ResponseEntity.notFound().build();
    }

    private int assembleLineNumber(String textLineNumber) {
        return Integer.valueOf(textLineNumber) - 1;
    }

    private boolean isValidForProcessing(int lineNumber) {
        return lineNumber > -1;
    }

}
