package szczepanski.gerard.textprovider.boundary.server;

import szczepanski.gerard.textprovider.infrastructure.http.HttpServer;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/shutdown")
@RequiredArgsConstructor
class ServerController {

    private final HttpServer server;

    @PostMapping
    void shutdownServer() {
        server.shutdown();
    }

}
