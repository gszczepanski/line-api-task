package szczepanski.gerard.textprovider.domain.line;

import java.util.Optional;

public interface TextLineRepository {

    Optional<String> findBy(int lineNumber);

}
