package szczepanski.gerard.textprovider.infrastructure.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import szczepanski.gerard.textprovider.infrastructure.file.linereader.TextFileLineReader;
import szczepanski.gerard.textprovider.infrastructure.file.linereader.TextFilePointerToLineAssembler;
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinePointersBlockLoader;
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinesPointersBlocksMap;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.PropertyAccessor.FIELD;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;

@Configuration
class FileConfig {

    @Value("${text-file.path}")
    private String textFilePath;

    @Value("${text-file.pointers-map-path}")
    private String textFilePointersMapPath;

    @Bean
    @Primary
    ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(FIELD, ANY);
        return mapper;
    }

    @Bean
    TextFilePointerToLineAssembler textFilePointerToLineAssembler() {
        return new TextFilePointerToLineAssembler();
    }

    @Bean
    TextFileLineReader textFileLineReader(TextFilePointerToLineAssembler textFilePointerToLineAssembler) {
        return new TextFileLineReader(textFilePath, textFilePointerToLineAssembler);
    }

    @Bean
    FileLinePointersBlockLoader fileLinePointersBlockLoader(ObjectMapper objectMapper) {
        File pointersMapFile = new File(textFilePointersMapPath);
        String directoryPath = Optional.ofNullable(pointersMapFile.getParent())
                .orElse(EMPTY);

        return new FileLinePointersBlockLoader(directoryPath, objectMapper);
    }

    @Bean
    FileLinesPointersBlocksMap fileLinesPointersBlocksMap(ObjectMapper objectMapper) {
        return loadFileLinesPointersBlocksMap(objectMapper);
    }

    private FileLinesPointersBlocksMap loadFileLinesPointersBlocksMap(ObjectMapper objectMapper) {
        try {
            return objectMapper.readValue(new File(textFilePointersMapPath), FileLinesPointersBlocksMap.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
