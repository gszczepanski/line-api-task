package szczepanski.gerard.textprovider.infrastructure.file;

import szczepanski.gerard.textprovider.domain.line.TextLineRepository;
import szczepanski.gerard.textprovider.infrastructure.file.linereader.TextFileLineReader;
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinePointerAssembler;
import java.util.Optional;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;


import static com.google.common.base.Preconditions.checkArgument;

@Component
@RequiredArgsConstructor
class FileTextLineProvider implements TextLineRepository {

    private final FileLinePointerAssembler fileLinePointerAssembler;

    private final TextFileLineReader fileLineReader;

    @Override
    public Optional<String> findBy(int lineNumber) {
        checkArgument(lineNumber > -1, "Line number must be positive");

        return fileLinePointerAssembler.convertLineNumberToFileLinePointers(lineNumber)
                .flatMap(pointers -> fileLineReader.readLine(pointers));
    }

}
