package szczepanski.gerard.textprovider.infrastructure.file.linereader;

import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinePointer;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.Optional;

import lombok.RequiredArgsConstructor;


import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.nonNull;

@RequiredArgsConstructor
public class TextFileLineReader {

    private static final String READ_FILE_MODE = "r";

    private final String textFilePath;

    private final TextFilePointerToLineAssembler textFilePointerToLineAssembler;

    public Optional<String> readLine(FileLinePointer pointer) {
        checkArgument(nonNull(pointer), "pointer is null");

        RandomAccessFile randomAccessFile = createRandomAccessFile();
        return textFilePointerToLineAssembler.assembleFrom(randomAccessFile, pointer);
    }

    private RandomAccessFile createRandomAccessFile() {
        try {
            return new RandomAccessFile(textFilePath, READ_FILE_MODE);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
