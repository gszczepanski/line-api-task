package szczepanski.gerard.textprovider.infrastructure.file.linereader;

import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinePointer;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Optional;


import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.nonNull;

public class TextFilePointerToLineAssembler {

    Optional<String> assembleFrom(RandomAccessFile raf, FileLinePointer fileLinePointers) {
        checkArgument(nonNull(raf), "raf is null");
        checkArgument(nonNull(fileLinePointers), "fileLinePointers is null");

        long bytePointer = fileLinePointers.getLineStartBytePosition();
        try {
            raf.seek(bytePointer);
            return Optional.ofNullable(raf.readLine());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
