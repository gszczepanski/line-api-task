package szczepanski.gerard.textprovider.infrastructure.file.pointer;

import lombok.Value;

@Value
public class FileLinePointer {

    private final long lineStartBytePosition;

}
