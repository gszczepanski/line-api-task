package szczepanski.gerard.textprovider.infrastructure.file.pointer;

import java.util.Optional;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class FileLinePointerAssembler {

    private final FileLinesPointersBlocksMap fileLinesPointersBlocksMap;

    private final FileLinePointersBlockLoader fileLinePointersBlockLoader;

    public Optional<FileLinePointer> convertLineNumberToFileLinePointers(int lineNumber) {

        return fileLinesPointersBlocksMap.getPointersBlockForLineNumber(lineNumber)
                .map(pointersBlockName -> fileLinePointersBlockLoader.load(pointersBlockName))
                .filter(pointersBlock -> pointersBlock.isContainPointersForLine(lineNumber))
                .flatMap(pointersBlock -> pointersBlock.getStartBytePointerForTheLine(lineNumber))
                .map(lineBytePointer -> new FileLinePointer(lineBytePointer));
    }
}
