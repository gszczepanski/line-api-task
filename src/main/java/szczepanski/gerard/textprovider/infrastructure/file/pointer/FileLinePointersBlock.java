package szczepanski.gerard.textprovider.infrastructure.file.pointer;

import java.util.Optional;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor(access = PRIVATE)
@EqualsAndHashCode
public class FileLinePointersBlock {

    @Getter
    private final int firstLineNumber;

    @Getter
    private final int lastLineNumber;

    private final long[] linePointers;

    public static FileLinePointersBlock from(int firstLineNumber, int lastLineNumber, long[] linePointers) {
        checkArgument(firstLineNumber > -1, "firstLineNumber is negative");
        checkArgument(lastLineNumber > -1, "lastLineNumber is negative");
        checkArgument(linePointers.length > 0, "linePointers array length is zero");

        int minExpectedSize = firstLineNumber == lastLineNumber ? 2 : (lastLineNumber - firstLineNumber) + 1;
        checkArgument(linePointers.length >= minExpectedSize,
                format("linePointers size should be at least %d, found %d", minExpectedSize, linePointers.length));

        return new FileLinePointersBlock(firstLineNumber, lastLineNumber, linePointers);
    }

    public Optional<Long> getStartBytePointerForTheLine(int lineNumber) {
        validateLineNumberParam(lineNumber);

        if (isContainPointersForLine(lineNumber)) {
            int index = getIndexForLineNumber(lineNumber);
            return Optional.of(linePointers[index]);
        }
        return Optional.empty();
    }

    private int getIndexForLineNumber(int lineNumber) {
        return lineNumber - firstLineNumber;
    }

    public boolean isContainPointersForLine(int lineNumber) {
        validateLineNumberParam(lineNumber);
        return isLineNumberIncludedInBlock(lineNumber);
    }

    private boolean isLineNumberIncludedInBlock(int lineNumber) {
        return firstLineNumber <= lineNumber && lineNumber <= lastLineNumber;
    }

    public int size() {
        return (lastLineNumber - firstLineNumber) + 1;
    }

    private void validateLineNumberParam(int lineNumber) {
        checkArgument(lineNumber > -1, "Line number is negative");
    }

}
