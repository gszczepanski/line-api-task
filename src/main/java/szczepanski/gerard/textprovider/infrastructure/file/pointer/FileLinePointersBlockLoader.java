package szczepanski.gerard.textprovider.infrastructure.file.pointer;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;

import lombok.RequiredArgsConstructor;


import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@RequiredArgsConstructor
public class FileLinePointersBlockLoader {

    private final String mapDirectoryPath;

    private final ObjectMapper objectMapper;

    FileLinePointersBlock load(String pointersBlockName) {
        checkArgument(isNotBlank(pointersBlockName), "pointersBlockName is blank");

        String pointersBlockPath = getPointersBlockPathFrom(pointersBlockName);
        return readFileLinePointersBlock(pointersBlockPath);
    }

    private String getPointersBlockPathFrom(String pointersBlockName) {
        return isEmpty(mapDirectoryPath) ? pointersBlockName : format("%s/%s", mapDirectoryPath, pointersBlockName);
    }

    private FileLinePointersBlock readFileLinePointersBlock(String pointersBlockPath) {
        try {
            return objectMapper.readValue(new File(pointersBlockPath), FileLinePointersBlock.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
