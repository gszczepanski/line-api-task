package szczepanski.gerard.textprovider.infrastructure.file.pointer;

import java.util.List;
import java.util.Optional;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Value;


import static com.google.common.base.Preconditions.checkArgument;

@RequiredArgsConstructor
public class FileLinesPointersBlocksMap {

    @Getter
    private final int blockLinesSize;

    @Getter
    private final int totalLinesSize;

    private final List<FileLinePointersBlockInfo> pointersBlocks;

    public Optional<String> getPointersBlockForLineNumber(int lineNumber) {
        checkArgument(lineNumber > -1, "lineNumber is negative");
        if (totalLinesSize <= lineNumber) {
            return Optional.empty();
        }

        int index = lineNumber / blockLinesSize;
        return Optional.of(pointersBlocks.get(index).getFileLinesPointersFilename());
    }

    @Value
    public static class FileLinePointersBlockInfo {

        private final int firstLine;

        private final int lastLine;

        private final String fileLinesPointersFilename;
    }

}
