package szczepanski.gerard.textprovider.infrastructure.http;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class HttpServer implements ApplicationContextAware {

    private static final int SHUTDOWN_DELAY_TIME_MS = 5000;

    private final ExecutorService shutdownThreadPool = Executors.newSingleThreadExecutor();

    private ApplicationContext context;

    public void shutdown() {
        shutdownThreadPool.submit(() -> shutdownGracefully());
    }

    private void shutdownGracefully() {
        try {
            Thread.sleep(SHUTDOWN_DELAY_TIME_MS);
            ((ConfigurableApplicationContext) context).close();
            shutdownThreadPool.shutdownNow();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
