package szczepanski.gerard.filelinepointersgenerator

import spock.lang.Specification
import spock.lang.Unroll
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinesPointersBlocksMap

class FileLinePointersBlockGeneratorSpec extends Specification {

    private static final String TEXT_FILE_10_LINES = '/text-file/textFile10Lines.txt'
    private static final String TEXT_FILE_23_LINES = '/text-file/textFile23Lines.txt'

    int pointersBlockSize = 5

    FileLinePointersWriterFake fileLinePointersWriter

    FileLinePointersBlocksMapWriterFake fileLinePointersBlocksMapWriter

    FileLinePointersGenerator fileLinePointersGenerator

    def setup() {
        fileLinePointersWriter = new FileLinePointersWriterFake()
        fileLinePointersBlocksMapWriter = new FileLinePointersBlocksMapWriterFake()
        fileLinePointersGenerator = new FileLinePointersGenerator(
                pointersBlockSize, fileLinePointersWriter, fileLinePointersBlocksMapWriter
        )
    }

    def 'should throw exception when given text file to process is null'() {
        when:
            fileLinePointersGenerator.createFileLinePointersFor(null)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'textFile is null'
    }

    def 'should create 2 pointers block files for 10 lines file'() {
        given:
            File textFile = loadFile(TEXT_FILE_10_LINES)
        when:
            fileLinePointersGenerator.createFileLinePointersFor(textFile)
        then:
            fileLinePointersWriter.numberOfSavedFileLinePointers() == 2
    }

    def 'should create 5 pointers block files for 23 lines file'() {
        given:
            File textFile = loadFile(TEXT_FILE_23_LINES)
        when:
            fileLinePointersGenerator.createFileLinePointersFor(textFile)
        then:
            fileLinePointersWriter.numberOfSavedFileLinePointers() == 5
    }

    def 'should create 5 pointers block files which have valid lines ranges'() {
        given:
            File textFile = loadFile(TEXT_FILE_23_LINES)
        when:
            fileLinePointersGenerator.createFileLinePointersFor(textFile)
        then:
            fileLinePointersWriter.getSavedFileLinePointers(0).getFirstLineNumber() == 0
            fileLinePointersWriter.getSavedFileLinePointers(1).getFirstLineNumber() == 5
            fileLinePointersWriter.getSavedFileLinePointers(2).getFirstLineNumber() == 10
            fileLinePointersWriter.getSavedFileLinePointers(3).getFirstLineNumber() == 15
            fileLinePointersWriter.getSavedFileLinePointers(4).getFirstLineNumber() == 20
    }

    @Unroll
    def 'should calculate valid line byte pointers for given lines'() {
        given:
            File textFile = loadFile(TEXT_FILE_23_LINES)
        when:
            fileLinePointersGenerator.createFileLinePointersFor(textFile)
        then:
            fileLinePointersWriter.getSavedFileLinePointers(index).getStartBytePointerForTheLine(lineNumber).get() == expStartBytePointer
        where:
            index | lineNumber | expStartBytePointer
            0     | 3          | 36
            1     | 8          | 110
            2     | 14         | 195
            3     | 16         | 230
            4     | 20         | 314
    }

    @Unroll
    def 'should generate fileLinePointersBlocksMap with ranges corresponding with generated fileLinePointers'() {
        given:
            File textFile = loadFile(TEXT_FILE_23_LINES)
        when:
            fileLinePointersGenerator.createFileLinePointersFor(textFile)
        then:
            FileLinesPointersBlocksMap map = fileLinePointersBlocksMapWriter.getSavedMap()
            map.pointersBlocks.get(index).firstLine == fileLinePointersWriter.getSavedFileLinePointers(index).getFirstLineNumber()
        where:
            index << [0, 1, 2, 3, 4]
    }

    def 'should generate fileLinePointersBlocksMap with valid total lines size'() {
        given:
            File textFile = loadFile(TEXT_FILE_23_LINES)
        when:
            fileLinePointersGenerator.createFileLinePointersFor(textFile)
        then:
            FileLinesPointersBlocksMap map = fileLinePointersBlocksMapWriter.getSavedMap()
            map.totalLinesSize == 23
    }

    def 'should generate fileLinePointersBlocksMap with valid block size'() {
        given:
            File textFile = loadFile(TEXT_FILE_23_LINES)
        when:
            fileLinePointersGenerator.createFileLinePointersFor(textFile)
        then:
            FileLinesPointersBlocksMap map = fileLinePointersBlocksMapWriter.getSavedMap()
            map.blockLinesSize == pointersBlockSize
    }


    static File loadFile(String filename) {
        return new File(getClass().getResource(filename).getFile())
    }

}
