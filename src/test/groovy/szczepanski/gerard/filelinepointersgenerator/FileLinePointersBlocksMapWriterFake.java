package szczepanski.gerard.filelinepointersgenerator;

import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinesPointersBlocksMap;

import lombok.Getter;

@Getter
class FileLinePointersBlocksMapWriterFake extends FileLinePointersBlocksMapWriter {

    private FileLinesPointersBlocksMap savedMap;

    public FileLinePointersBlocksMapWriterFake() {
        super("test", null);
    }

    @Override
    void saveToDisk(FileLinesPointersBlocksMap map) {
        this.savedMap = map;
    }
}
