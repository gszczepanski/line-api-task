package szczepanski.gerard.filelinepointersgenerator;

import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinePointersBlock;
import java.util.ArrayList;
import java.util.List;

class FileLinePointersWriterFake extends FileLinePointersWriter {

    private final List<FileLinePointersBlock> savedPointers = new ArrayList<>();

    public FileLinePointersWriterFake() {
        super("fake", null);
    }

    FileLinePointersBlock getSavedFileLinePointers(int index) {
        return savedPointers.get(index);
    }

    int numberOfSavedFileLinePointers() {
        return savedPointers.size();
    }

    @Override
    String saveToDisk(FileLinePointersBlock pointers, int blockNumber) {
        savedPointers.add(pointers);
        return String.format("test/p-%d.json", blockNumber);
    }
}
