package szczepanski.gerard.textprovider

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

@SpringBootTest(classes = [TextProviderApiConfig.class])
@ActiveProfiles("integrationtest")
abstract class IntegrationSpec extends Specification {

    @Autowired
    WebApplicationContext context

    MockMvc mvc

    def setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build()
    }

}
