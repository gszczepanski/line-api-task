package szczepanski.gerard.textprovider.boundary.line


import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import spock.lang.Unroll
import szczepanski.gerard.textprovider.IntegrationSpec

import static java.lang.String.format
import static org.hamcrest.Matchers.containsString
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class TextLineControllerSpec extends IntegrationSpec {

    @Unroll
    def 'should return text line for given lineNumber with 200 http response'() {
        expect:
            this.mvc.perform(getLineRequest(lineNumber))
                    .andDo(print()).andExpect(status().isOk())
                    .andExpect(content().string(containsString(expectedLine)))
        where:
            lineNumber   || expectedLine
            '1'          || '[0] Consequences, the advantage painful. Pleasure him ever the man.'
            '5'          || '[4] Produces enjoy to the give.'
            '75'         || '[74] Us from avoids.'
            '165'        || '[164] Right idea that actual.'
            '445'        || '[444] It? the account painful.'
            '775'        || '[774] A a pain rationally pleasure chooses pleasure.'
            '885'        || '[884] I has some it? which this.'
            '900'        || '[899] Ever some No.'
            '1000'       || '[999] Know that you fault pain obtain teachings right happiness.'
    }

    def 'should return 404 http response for not existing line number'() {
        expect:
            this.mvc.perform(getLineRequest('6666'))
                    .andDo(print()).andExpect(status().isNotFound())
    }

    def 'should return 404 http response for not numeric line number'() {
        expect:
            this.mvc.perform(getLineRequest('not-numeric-for-sure'))
                    .andDo(print()).andExpect(status().isNotFound())
    }

    def 'should return 404 http response for zero line number'() {
        expect:
            this.mvc.perform(getLineRequest('0'))
                    .andDo(print()).andExpect(status().isNotFound())
    }

    def 'should return 404 http response for negative line number'() {
        expect:
            this.mvc.perform(getLineRequest('-1'))
                    .andDo(print()).andExpect(status().isNotFound())
    }

    private MockHttpServletRequestBuilder getLineRequest(String lineNumber) {
        return get(format('/api/lines/%s', lineNumber));
    }
}
