package szczepanski.gerard.textprovider.infrastructure.file.linereader

import spock.lang.Specification
import spock.lang.Unroll
import szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinePointer

class TextFileLineReaderSpec extends Specification {

    String textFilePath = 'src/test/resources/text-file/textFile2Lines.txt'

    TextFilePointerToLineAssembler textFilePointerToLineAssembler = new TextFilePointerToLineAssembler()

    TextFileLineReader textFileLineReader = new TextFileLineReader(
            textFilePath, textFilePointerToLineAssembler
    )

    def 'should throw exception when given pointer is null'() {
        when:
            textFileLineReader.readLine(null)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'pointer is null'
    }

    @Unroll
    def 'should return read line starting from byte pointed by pointer'() {
        given:
            FileLinePointer pointer = new FileLinePointer(lineBytePosition)
        when:
            Optional<String> line = textFileLineReader.readLine(pointer)
        then:
            line.isPresent()
            line.get() == expectedLine
        where:
            lineBytePosition || expectedLine
            0L               || '[1] First Line'
            15L              || '[2] Second Line'
    }

    def 'should return empty optional for pointer that exceeds file size'() {
        given:
            FileLinePointer pointer = new FileLinePointer(10000L)
        when:
            Optional<String> line = textFileLineReader.readLine(pointer)
        then:
            !line.isPresent()
    }
}
