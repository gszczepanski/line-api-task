package szczepanski.gerard.textprovider.infrastructure.file.pointer

import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Specification

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY
import static com.fasterxml.jackson.annotation.PropertyAccessor.FIELD

class FileLinePointersBlockLoaderSpec extends Specification {

    String mapDirectoryPath = 'src/test/resources/pointers'

    ObjectMapper objectMapper = createObjectMapper()

    FileLinePointersBlockLoader loader = new FileLinePointersBlockLoader(mapDirectoryPath, objectMapper)

    def 'should load FileLinePointersBlock for the given file line pointers block name'() {
        given:
            String pointersBlockName = 'flp-0.json'
        when:
            FileLinePointersBlock block = loader.load(pointersBlockName)
        then:
            block.getFirstLineNumber() == 0
            block.getLastLineNumber() == 99
    }

    def 'should throw exception when given pointersBlockName is null for load FileLinePointersBlock'() {
        when:
            loader.load(null)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'pointersBlockName is blank'
    }

    def 'should throw exception when given pointersBlockName is blank for load FileLinePointersBlock'() {
        when:
            loader.load('      ')
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'pointersBlockName is blank'
    }

    private static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper()
        mapper.setVisibility(FIELD, ANY)
        return mapper
    }

}
