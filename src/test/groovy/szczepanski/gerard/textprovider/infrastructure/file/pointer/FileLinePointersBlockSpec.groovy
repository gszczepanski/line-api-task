package szczepanski.gerard.textprovider.infrastructure.file.pointer

import spock.lang.Specification
import spock.lang.Unroll

class FileLinePointersBlockSpec extends Specification {

    def 'should create FileLinePointers for startLine, lastLine and array of pointers'() {
        given:
            int firstLineNumber = 731
            int lastLineNumber = 734
            long[] linePointers = [6677L, 6690L, 6712L, 6743L, 6748L]
        when:
            FileLinePointersBlock pointers = FileLinePointersBlock.from(firstLineNumber, lastLineNumber, linePointers)
        then:
            pointers.isContainPointersForLine(731)
            pointers.isContainPointersForLine(732)
            pointers.isContainPointersForLine(733)
            pointers.isContainPointersForLine(734)
            !pointers.isContainPointersForLine(735)
    }

    def 'should throw exception during create FileLinePointers when firstLineNumber is negative'() {
        given:
            int lastLineNumber = 734
            long[] linePointers = [6677L, 6690L, 6712L, 6743L, 6748L]
        when:
            FileLinePointersBlock.from(-1, lastLineNumber, linePointers)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'firstLineNumber is negative'
    }

    def 'should throw exception during create FileLinePointers when lastLineNumber is negative'() {
        given:
            int firstLineNumber = 731
            long[] linePointers = [6677L, 6690L, 6712L, 6743L, 6748L]
        when:
            FileLinePointersBlock.from(firstLineNumber, -1, linePointers)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'lastLineNumber is negative'
    }

    def 'should throw exception during create FileLinePointers when linePointers array is empty'() {
        given:
            int firstLineNumber = 731
            int lastLineNumber = 734
            long[] linePointers = []
        when:
            FileLinePointersBlock.from(firstLineNumber, lastLineNumber, linePointers)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'linePointers array length is zero'
    }

    def 'should throw exception during create FileLinePointers when linePointers array has does not meet expected size'() {
        given:
            int firstLineNumber = 731
            int lastLineNumber = 734
            long[] linePointers = [6677L, 6690L, 6712L]
        when:
            FileLinePointersBlock.from(firstLineNumber, lastLineNumber, linePointers)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'linePointers size should be at least 4, found 3'
    }

    def 'should throw exception when line number for getStartBytePointerForTheLine is negative'() {
        given:
            long[] linePointers = [0L, 10L]
            FileLinePointersBlock pointers = FileLinePointersBlock.from(0, 0, linePointers)
        when:
            pointers.getStartBytePointerForTheLine(-1)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'Line number is negative'
    }

    @Unroll
    def 'should return start byte pointer for the line when line start byte pointer is present'() {
        given:
            long[] linePointers = [100L, 200L, 205L, 290L]
            FileLinePointersBlock pointers = new FileLinePointersBlock(709, 711, linePointers)
        when:
            Optional<Long> startBytePointer = pointers.getStartBytePointerForTheLine(lineNumber)
        then:
            startBytePointer.get() == expectedStartBytePointer
        where:
            lineNumber || expectedStartBytePointer
            709        || 100L
            710        || 200L
            711        || 205L
    }

    def 'should return empty optional when when line start byte pointer is not present'() {
        given:
            long[] linePointers = [0L, 5L, 20L, 45L]
            FileLinePointersBlock pointers = new FileLinePointersBlock(0, 2, linePointers)
        when:
            Optional<Long> startBytePointer = pointers.getStartBytePointerForTheLine(3)
        then:
            !startBytePointer.isPresent()
    }

    def 'should throw exception when line number for getLastBytePointerForTheLine is negative'() {
        given:
            long[] linePointers = [0L, 10L]
            FileLinePointersBlock pointers = new FileLinePointersBlock(0, 0, linePointers)
        when:
            pointers.getStartBytePointerForTheLine(-1)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'Line number is negative'
    }

}
