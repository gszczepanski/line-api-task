package szczepanski.gerard.textprovider.infrastructure.file.pointer

import spock.lang.Specification
import spock.lang.Unroll

import static szczepanski.gerard.textprovider.infrastructure.file.pointer.FileLinesPointersBlocksMap.FileLinePointersBlockInfo

class FileLinesPointersBlocksMapSpec extends Specification {

    private static final int BLOCK_LINES_SIZE = 5
    private static final int TOTAL_LINES_SIZE = 23

    List<FileLinePointersBlockInfo> pointersBlocks = [
            new FileLinePointersBlockInfo(0, 4, 'flp-0.json'),
            new FileLinePointersBlockInfo(5, 9, 'flp-1.json'),
            new FileLinePointersBlockInfo(10, 14, 'flp-2.json'),
            new FileLinePointersBlockInfo(15, 19, 'flp-3.json'),
            new FileLinePointersBlockInfo(20, 22, 'flp-4.json')
    ]

    FileLinesPointersBlocksMap map = new FileLinesPointersBlocksMap(BLOCK_LINES_SIZE, TOTAL_LINES_SIZE, pointersBlocks)

    def 'should throw exception when given lineNumber for getPointersBlockForLineNumber is negative'() {
        when:
            map.getPointersBlockForLineNumber(-1)
        then:
            IllegalArgumentException ex = thrown()
            ex.message == 'lineNumber is negative'
    }

    @Unroll
    def 'should return valid pointers block filename for given line number'() {
        when:
            Optional<String> fileName = map.getPointersBlockForLineNumber(lineNumber)
        then:
            fileName.get() == expectedFileName
        where:
            lineNumber || expectedFileName
            0          || 'flp-0.json'
            1          || 'flp-0.json'
            2          || 'flp-0.json'
            3          || 'flp-0.json'
            4          || 'flp-0.json'
            5          || 'flp-1.json'
            6          || 'flp-1.json'
            7          || 'flp-1.json'
            8          || 'flp-1.json'
            9          || 'flp-1.json'
            10         || 'flp-2.json'
            11         || 'flp-2.json'
            12         || 'flp-2.json'
            13         || 'flp-2.json'
            14         || 'flp-2.json'
            15         || 'flp-3.json'
            16         || 'flp-3.json'
            17         || 'flp-3.json'
            18         || 'flp-3.json'
            19         || 'flp-3.json'
            20         || 'flp-4.json'
            21         || 'flp-4.json'
            22         || 'flp-4.json'
    }

    def 'should return empty optional when given line number is higher or equal total number of lines'() {
        when:
            Optional<String> fileName = map.getPointersBlockForLineNumber(TOTAL_LINES_SIZE)
        then:
            !fileName.isPresent()
    }
}
